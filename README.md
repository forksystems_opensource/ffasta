# ffasta

The ffasta tool is a simple python program for extraction records from fasta files based on a description of the sequence. 

Author: Konrad J. Debski [ FORK SYSTEMS ]

Licence: It is as it is. Use it if you need it :) Rememver that you are is it on your own risk.

# Help
```
usage: ffasta [-h] [--version] [--fasta FASTA] [--overwrite] [--output OUTPUT] [--debug] [--ids IDS] [--pattern PATTERN]

The ffasta program filter fasta files using regex pattern and ids.

options:
  -h, --help            show this help message and exit
  --version, -v         show program's version number and exit
  --fasta FASTA, -f FASTA
                        Input fasta file
  --overwrite           Overwrites existing file
  --output OUTPUT       Path to output fasta file
  --debug               Debug mode :)
  --ids IDS             Comma separated ids
  --pattern PATTERN     Pattern to filter sequences by id

Author: Konrad J. Debski [ FORK SYSTEMS ]
```
