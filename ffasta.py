import textwrap
import re
import logging
import gzip
import os
from collections import OrderedDict
import argparse
from tqdm import tqdm
import sys

class FileAlreadyExists(Exception):
    pass

class FileDoesNotExist(Exception):
    pass

# SINGLETON
class Singleton(type):
    '''
    Singleton class
    '''
    _instances = {}
    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]

class Log(metaclass=Singleton):
    '''
    Log singleton class

    author: Konrad J. Debski
    '''
    def __init__(self):
        logger = logging.getLogger(__name__)
        logger.setLevel(logging.DEBUG)
        c_handler = logging.StreamHandler()
        c_handler.setLevel(logging.DEBUG)
        c_format = logging.Formatter('[%(asctime)s] [%(levelname)s] - %(message)s')
        c_handler.setFormatter(c_format)
        logger.addHandler(c_handler)
        self._logger = logger
    def error(self, msg: str):
        self._logger.error(msg)
    def warning(self, msg: str):
        self._logger.warning(msg)
    def info(self, msg: str):
        self._logger.info(msg)
    def debug(self, msg: str):
        self._logger.debug(msg)
    def critical(self, msg: str):
        self._logger.critical(msg)


def wrap_sequence(sequence, width = 80):
    '''
    Wrap sequence

    :param sequence: sequence to wrap
    :param width: line width

    :return wrapped string:
    author: Konrad J. Debski
    '''
    assert isinstance(sequence, str)
    assert isinstance(width, int)
    return '\n'.join(
        textwrap.wrap(
            sequence, 
            width
        )
    )


def save_to_file(x, path, overwrite = False):
    '''
    Save list to text file

    :param x: list of strings
    :param path: path to output file if ends with '.gz' it will be compressed
    :param overwrite: flag. If True existing file will be overwritten.

    :return nothing:
    author: Konrad J. Debski
    '''
    log = Log()
    assert isinstance(x, list)
    assert all([isinstance(e, str) for e in x])
    assert isinstance(path, str)
    assert isinstance(overwrite, bool)
    if os.path.isfile(path) and not overwrite:
        err = f"File {path} already exist"
        log.error(err)
        raise FileAlreadyExists(err)
    if path.endswith('.gz'):
        mode = 'wb'
        open_fun = gzip.open
    else:
        mode = 'w'
        open_fun = open
    with open_fun(path, mode) as f:
        for e in x:
            if 'b' in mode:
                e = e.encode("utf-8")
            f.write(e)


class FastaRecord:
    '''
    FastaRecord class to manipulate single sequence in fasta file
    '''
    def __init__(self, id = None, seq = ""):
        self._id = id
        self._seq = ""
    @property
    def id(self):
        return self._id
    @id.setter
    def id(self, id):
        self._id = id
    @property
    def sequence(self):
        return self._seq
    @sequence.setter
    def sequence(self, seq):
        self._seq = seq
    def append(self, seq):
        assert isinstance(seq, str)
        self._seq = self._seq + seq
    def wrapped_sequence(self, width = 80):
        return wrap_sequence(self.sequence, width)
    def fasta(self, width = 80):
        return f">{self.id}\n{self.wrapped_sequence(width)}\n"
    def __repr__(self):
        return self.fasta(80).rstrip()
    def has_id_match(self, pattern):
        assert isinstance(pattern, str)
        return bool(re.match(pattern, self.id))
    def has_id(self, id):
        assert isinstance(id, str)
        return id == self.id
    @property
    def has_sequence(self):
        return self.sequence != ""


class Records:
    '''
    Records class to work with set of FastaRecord objects
    '''
    _log = Log()
    def __init__(self):
        self._records = OrderedDict()
    def add(self, record, overwrite = True):
        if record.id in self._records.keys():
            if overwrite:
                self._log.warning(f'Overwriting sequence "{record.id}"')
            else:
                err = f'Sequence "{record.id}" already added'
                self._log.error(err)
                raise Exception(err)
        self._records[record.id] = record
    def drop(self, id):
        if id not in self._records.keys():
            self._log.warning("Nothing to remove")
        else:
            del self._records[id]
    def has_id(self, id):
        return id in self._records.keys()
    def has_ids(self, ids):
        return OrderedDict({
            e:e in self._records.keys() 
            for e in ids
        })
    def has_id_match(self, pattern):
        return OrderedDict({
            e:bool(re.match(pattern, e)) 
            for e in self._records.keys()
        })
    def get(self, id):
        if not self.has_id(id):
            err = f'Unknown sequence "{id}" is not in records'
            self._log.error(err)
            raise Exception(err)
        return self._records[id]
    @property
    def ids(self):
        return list(self._records.keys())
    def save_to_fasta(self, path, ids = None, pattern = None, overwrite = False):
        if not self._records:
            err = "Nothing to save"
            self._log.error(err)
            raise Exception(err)
        if ids and pattern:
            err = "Arguments conflict. Please, use pattern or ids argument not both."
            self._log.error(err)
            raise Exception(err)
        if ids == None and pattern == None:
            self._log.info("Removing records without sequnce")
            fastas = [e.fasta(80) for e in self._records.values() if e.has_sequence]
        elif ids:
            has_ids = self.has_ids(ids)
            missing_ids = [k for k, v in has_ids.items() if not v]
            if missing_ids:
                err = f'Unknown ids: {", ".join(missing_ids)}'
                self._log.error(err)
                raise Exception(err)
            else:
                fastas = [e.fasta(80) for e in self._records.values() if e.id in ids]
        elif pattern:
            self._log.info("Checking if any of sequences ids match the pattern")
            has_ids_pattern = self.has_id_match(pattern)
            if not any(has_ids_pattern.values()):
                err = f'IDs does not match the pattern: "{pattern}"'
                self._log.error(err)
                raise Exception(err)
            else:
                self._log.info("Matched...")
                pattern_ids = [k for k, v in has_ids_pattern.items() if v]
                self._log.info("Extracting...")
                fastas = [e.fasta(80) for e in tqdm(self._records.values()) if e.id in pattern_ids]
        log.info(f"Saving {len(fastas)} sequences")
        save_to_file(fastas, path, overwrite)


def read_fasta(path):
    '''
    Read fasta file

    :param path: path to fasta file

    :return Records object:
    author: Konrad J. Debski
    '''
    log = Log()
    if not os.path.isfile(path):
        err = f"File does not exist: {path}"
        log.error(err)
        raise FileDoesNotExist(err)
    log.info(f"Reading fasta file... {path}")
    with open(path, 'r') as f:
        lines = f.readlines()
    log.info("Preprocessing fasta lines...")
    if lines:
        records = Records()
        id = None
        fasta_record = None
        for line in tqdm(lines):
            line = line.rstrip()
            if re.match("^>", line):
                id = re.sub("^>", "", line)
                if fasta_record != None:
                    records.add(fasta_record)
                fasta_record = FastaRecord(id)
            else:
                fasta_record.append(line)
        return records

def main(fasta_path, output, ids = None, pattern = None, overwrite = False):
    fasta = read_fasta(fasta_path)
    fasta.save_to_fasta(output, ids = ids, pattern = pattern)


                
if __name__ == "__main__":
    sys.tracebacklimit = 0
    VERSION = "0.0.1"
    log = Log()
    parser = argparse.ArgumentParser(
        prog = "ffasta",
        description = "The ffasta program filter fasta files using regex pattern and ids.",
        epilog = "Author: Konrad J. Debski [ FORK SYSTEMS ]"
    )
    parser.add_argument(
        '--version',
        '-v',
        action = 'version',
        version = f'%(prog)s {VERSION}'
    )
    parser.add_argument(
        '--fasta',
        '-f',
        action = 'store',
        help = 'Input fasta file'
    )
    parser.add_argument(
        '--overwrite',
        action = 'store_true',
        help = 'Overwrites existing file',
        default = False
    )
    parser.add_argument(
        '--output',
        action = 'store',
        help = 'Path to output fasta file'
    )
    parser.add_argument(
        '--debug',
        action = 'store_true',
        help = 'Debug mode :)'
    )
    parser.add_argument(
        '--ids',
        action = 'store',
        help = "Comma separated ids"
    )
    parser.add_argument(
        '--pattern',
        action = 'store',
        help = "Pattern to filter sequences by id"
    )
    #
    args = parser.parse_args()
    if args.debug:
        sys.tracebacklimit = 1
    #
    if not args.ids and not args.pattern:
        err = "One of the arguments is required --ids or --pattern"
        log.error(err)
        raise Exception(err)
    ids = None
    if args.ids:
        ids = args.ids.split(',')
    main(
        fasta_path = args.fasta,
        output = args.output,
        ids = ids,
        pattern = args.pattern,
        overwrite = args.overwrite
    )
